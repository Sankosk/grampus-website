from django.shortcuts import render_to_response
from django.template import RequestContext
from website.apps.bug_reporter.forms import report_a_bug
from website.apps.bug_reporter.models import reporter

def bug_reporter_view(request):
	if request.method == "POST":
		form = report_a_bug(request.POST)
		info_sent = "Initializing"
		if form.is_valid():
			name = form.cleaned_data['Name']
			email = form.cleaned_data['Email']
			application = form.cleaned_data['Application']
			description = form.cleaned_data['Description']

			r = reporter()
			r.name = name
			r.email = email
			r.application = application
			r.description = description
			r.save()
			info_sent = "The bug has been reported correctly"

		else:
			info_sent = "An error has ocurred and the bug has not been reported correctly"

		form = report_a_bug()
		ctx = {'form':form, 'info':info_sent}
		return render_to_response('home/bugreporter.html', ctx, context_instance=RequestContext(request))

	else:
		form = report_a_bug()
		ctx = {'form':form}



	return render_to_response('home/bugreporter.html', ctx, context_instance=RequestContext(request))
