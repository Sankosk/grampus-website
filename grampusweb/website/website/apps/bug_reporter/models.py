from django.db import models

class reporter(models.Model):
	#name = Reporter's name
	#application = Name of the BUGGED app
	#description = to explain the bug
	name = models.CharField(max_length=100)
	email = models.EmailField()
	application = models.CharField(max_length=100)
	description = models.TextField(max_length=1000)

	def __unicode__(self):
		to_show = "%s in %s"%(self.name, self.application)
		return to_show



