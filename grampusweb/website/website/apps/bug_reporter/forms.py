from django import forms

class report_a_bug(forms.Form):
	Name = forms.CharField(widget=forms.TextInput())
	Email = forms.EmailField(widget=forms.TextInput())
	Application = forms.CharField(widget=forms.TextInput())
	Description = forms.CharField(widget=forms.Textarea())

	def clean(self):
		return self.cleaned_data