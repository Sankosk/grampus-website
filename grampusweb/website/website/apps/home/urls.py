from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('website.apps.home.views',
	url(r'^$', 'index_view', name='principal_view'),
	url(r'^projects/$', 'projects_view', name='ourprojects_view'),
	url(r'^services/$', 'services_view', name='ourservices_view'),
	url(r'^partners/$', 'partners_view', name='ourpartners_view'),
	url(r'^contactus/$', 'contact_view', name='contactus_view'),
	url(r'^gallery/$', 'gallery_view', name='ourgallery_view'),
	)