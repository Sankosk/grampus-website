from django.shortcuts import render_to_response
from django.template import RequestContext
from website.apps.home.forms import ContactForm
from django.core.mail import EmailMultiAlternatives

def index_view(request):
	return render_to_response('home/index.html', context_instance=RequestContext(request))

def projects_view(request):
	return render_to_response('home/projects.html', context_instance=RequestContext(request))

def services_view(request):
	return render_to_response('home/services.html', context_instance=RequestContext(request))

def partners_view(request):
	return render_to_response('home/partners.html', context_instance=RequestContext(request))

def contact_view(request):
	info_sent = False
	name = ""
	email = ""
	subject = ""
	message = ""

	if request.method == "POST":
		formulary = ContactForm(request.POST)
		if formulary.is_valid():
			info_sent = True
			name = formulary.cleaned_data['Name']
			email = formulary.cleaned_data['Email']
			subject = formulary.cleaned_data['Subject']
			message = formulary.cleaned_data['Message']

			#Enviando mensaje via GMAIL
			to_grampus = 'grampusteam@gmail.com'
			html_content = "Has recibido un mensaje de %s , con el email %s <br><br>Asunto: %s <br><br>Mensaje: <br>%s"%(name, email, subject, message)
			msg = EmailMultiAlternatives('Contact email', html_content, 'from@server.com', [to_grampus])
			msg.attach_alternative(html_content, 'text/html')
			msg.send()
	else:
		formulary = ContactForm()

	ctx = {'form':formulary, 'name':name, 'email':email, 'subject':subject, 'message':message, 'info_sent':info_sent}
	return render_to_response('home/contactus.html', ctx, context_instance=RequestContext(request))

def gallery_view(request):
	return render_to_response('home/gallery/gallery.html', context_instance=RequestContext(request))
